#!/usr/bin/env bash

#Thanks to https://cloudnull.io/author/kevin/

#if command -v python3 &>/dev/null; then
#    echo Python 3 is installed
#else
#    echo Python 3 is not installed
#    break
#fi

#if python -c 'import pkgutil; exit(not pkgutil.find_loader("python-openstackclient"))'; then
#    echo 'python-openstackclient found'
#else
#    echo 'python-openstackclient not found'
#fi

#if python -c 'import pkgutil; exit(not pkgutil.find_loader("python-neutronclient"))'; then
#    echo 'python-neutronclient found'
#else
#    echo 'python-neutronclient not found'
#fi

printf "\nDelete Openstack LoadBalancer Script\nYou should already logged in python-openstack tools\nIf you need more information please visit http://bit.ly/2Spo5\n\n\n"

# Get Load Balancher ID form parameter or user input
if [ $# -eq 0 ]
  then
    printf "Geting LoadBalancer List\n "
    neutron lbaas-loadbalancer-list 2>null
    printf "\n\nWhich LoadBalacher you want to delete? Please Input Load Balancer ID or CTRL+C to cancel: "
    read LB_ID
else
  LB_ID=$1
fi

# delete the lb and all sub components.
printf "\n\nGeting LoadBalancer Data\n"
LB_DATA=$(neutron lbaas-loadbalancer-show ${LB_ID} 2>null)
printf "Geting Listeners Data\n"
LB_LISTENERS_ID=$(echo -e "$LB_DATA" | awk -F'"' '/listeners/ {print $4}')
printf "Geting Pools Data\n"
LB_POOL_ID=$(echo -e "$LB_DATA" | awk -F'"' '/pools/ {print $4}')
printf "Geting Health Monitor Data\n"
LB_HEALTH_ID=$(neutron lbaas-pool-show ${LB_POOL_ID} 2>null| awk '/healthmonitor_id/ {print $4}')
printf "\n\nDeleting...\n"
neutron lbaas-listener-delete "${LB_LISTENERS_ID}" 2>null
neutron lbaas-healthmonitor-delete "${LB_HEALTH_ID}" 2>null
neutron lbaas-pool-delete "${LB_POOL_ID}" 2>null
neutron lbaas-loadbalancer-delete "${LB_ID}" 2>null